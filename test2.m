clear;clc
% numc = number of controls
numc=1;
% FT = Final time
FT=1;
% dt = Timestep
dt=0.1;

% BOUNDARY POINTS
a = 0;
b = 1;
dx = 0.0001;
% vec = Time array
vec=0:dt:FT;
space=a:dx:b;


for i=1:length(vec)
    for j=1:length(space)
        ConVec(i,j,1)=(pi^2)*cos(pi*space(j))*exp(vec(i));
    end
end

%i=1;j=100;
%for i=1:length(vec)
  %  for j=1:length(space)
        x = dx+(dx/2);i=8;
        c1=interp1(space,ConVec(i,:,1),x,'spline');
        c2=(pi^2)*cos(pi*x)*exp(vec(i));
        c3=norm(c1-c2)
 %   end
%end
%surf(c3)