function dy=CoState(t,y,VarVec,ConVec,rho)
dx=0.1;
dy=zeros(size(y));
for i=1:length(VarVec)
    temp0 = VarVec;
    temp0(i)=VarVec(i)+dx;    
    [temp_dx1, obj1]=State(t,temp0,ConVec);
    temp = 0;
    for j=1:length(VarVec)
        temp = temp + y(j)*temp_dx1(j);
    end
    temp_main1 = obj1+temp;
    
    temp1 = VarVec;
    temp1(i)=VarVec(i)-dx;    
    [temp_dx2, obj2]=State(t,temp1,ConVec);
    tempz = 0;
    for j=1:length(VarVec)
        tempz = tempz + y(j)*temp_dx2(j);
    end
    temp_main2 = obj2+tempz;    
    
    dy(i) = (temp_main1-temp_main2)/(2*dx);
end
dy=dy-rho*y;
