function Y=ODEBackward(ConVec,ya,dt,vec,var,num,ODEFinal,rho,MatlabSolver,tol)
%
reltol=tol(1);abstol=tol(2);
Y=ones(var,length(vec));
VarVec=zeros(1,var);  % VarVec = State, Co-State variable vector
ConVect=zeros(num);  % ConVect = Control variable vector
Y(:,length(vec))=ODEFinal;
for i=2:length(vec)
    kk=length(vec)+1-i;
    for j=1:var
        VarVec(j)=ya(j,kk+1);
    end
    for j=1:num
        ConVect(j)=ConVec(j,kk+1);
    end
    %time = vec(i);
    options = odeset('RelTol',reltol,'AbsTol',abstol,'MaxStep',0.1);
    ff  = @(t,y)CoState(t,y,VarVec,ConVect,rho);
    temp=feval(MatlabSolver,ff,[vec(i-1) vec(i)],ODEFinal,options); 
    ZZ=deval(temp,vec(i));    
    ODEFinal=ZZ;
    for j=1:var
        Y(j,kk)=ZZ(j);
    end
end