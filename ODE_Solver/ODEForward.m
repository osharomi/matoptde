function Y=ODEForward(ConVec,dt,vec,var,num,ODEInit,MatlabSolver,tol)
%
reltol=tol(1);abstol=tol(2);
Y=zeros(var,length(vec));
ConVect=zeros(num);  % ConVec = Control variable vector
Y(:,1)=ODEInit;
for i=2:length(vec)
    for j=1:num
        ConVect(j)=ConVec(j,i-1);
    end
    %time = vec(i);
    options = odeset('RelTol',reltol,'AbsTol',abstol,'MaxStep',0.1);
    ff  = @(t,y)State(t,y,ConVect);
    temp=feval(MatlabSolver,ff,[vec(i-1) vec(i)],ODEInit,options);
    ZZ=deval(temp,vec(i)); 
    ODEInit=ZZ;
    for j=1:var
        Y(j,i)=ZZ(j);
    end
end
