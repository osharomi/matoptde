function Y=PDEBackward2(ConVect,ya,A,vec,var,num,space,DF,PDEFinal,rho,tol)
% Setting up the Neumann Boundary Condition Differentiation matrix
reltol=tol(1);abstol=tol(2);
Y=zeros(length(vec),length(space),var);
VarVec=zeros(length(space),var);  % VarVec = State, Co-State variable vector
ConVec=zeros(length(space),num);  % ConVec = Control variable vector
Y(length(vec),:,:)=PDEFinal;
for i=2:length(vec)
    kk=length(vec)+1-i;
    for j=1:var
        VarVec(:,j)=ya(kk+1,:,j);
    end
    for j=1:num
        ConVec(:,j)=ConVect(kk+1,:,j);
    end
    for j=1:var
        ff = @(t,y) (DF(j)*A*y + CoState2(0,y,space,ConVec,VarVec,var,rho,t));
        options = odeset('RelTol',reltol,'AbsTol',abstol,'MaxStep',0.1);
        temp=feval('ode15s',ff,[vec(i-1) vec(i)],PDEFinal(:,j),options);
        temp=deval(temp,vec(i));
        PDEFinal(:,j) = temp;
    end
    for j=1:var
        Y(i,:,j)=PDEFinal;
    end    
end




% function Y=PDEBackward(ConVect,ya,dt,vec,var,num,space,DF,PDEFinal,rho,tol)
% %---------------------------------
% reltol=tol(1);abstol=tol(2);
% Y=zeros(length(vec),length(space),var);
% VarVec=zeros(length(space),var);  % VarVec = State, Co-State variable vector
% ConVec=zeros(length(space),num);  % ConVec = Control variable vector
% Y(length(vec),:,:)=PDEFinal;
% for i=2:length(vec)
%     kk=length(vec)+1-i;
%     for j=1:var
%         VarVec(:,j)=ya(kk+1,:,j);
%     end
%     for j=1:num
%         ConVec(:,j)=ConVect(kk+1,:,j);
%     end
%     x = space; t = linspace(vec(i-1),vec(i),3);
%     options = odeset('RelTol',reltol,'AbsTol',abstol,'NormControl','on');
%     temp = pdepe(0,@(x,t,u,DuDx)pde(x,t,u,DuDx,var,space,DF,...
%                  ConVec,VarVec,rho,PDEFinal),@(x)pdeic(x,var,...
%                  space,PDEFinal),@(xl,ul,xr,ur,t)pdebc(xl,ul,...
%                  xr,ur,t,var),x,t,options);
%     for j=1:var
%         PDEFinal(:,j) = temp(end,:,j);
%         Y(kk,:,j)=temp(end,:,j);
%     end
% end
% 
% 
% % --------------------------------------------------------------
% function [c,f,s] = pde(x,t,u,DuDx,var,space,DF,ConVec,VarVec,rho,PDEFinal)
% c = ones(var,1); 
% f = DF .* DuDx; 
% s = CoState(x,u,space,ConVec,VarVec,var,rho,PDEFinal,t);
% % --------------------------------------------------------------
% function u0 = pdeic(x,~,space,G)
% for i=1:length(space)
%     if x==space(i)
%         u0=G(i,:);
%     end
% end
% u0=u0';
% % --------------------------------------------------------------
% function [pl,ql,pr,qr] = pdebc(~,~,~,~,~,var)
% pl = zeros(var,1); 
% ql = ones(var,1); 
% pr = zeros(var,1); 
% qr = ones(var,1);