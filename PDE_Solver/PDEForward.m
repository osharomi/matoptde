function Y=PDEForward(ConVect,A,vec,var,num,space,DF,PDEInit,tol,method,MatlabSolver)
% -------------------------------
reltol=tol(1);abstol=tol(2);
Y=zeros(length(vec),length(space),var);
ConVec=zeros(length(space),num);  % ConVec = Control variable vector
Y(1,:,:)=PDEInit;

switch method
    case 'matlabpdepe'
            for i=2:length(vec)
                for j=1:num
                    ConVec(:,j)=ConVect(i-1,:,j);
                end
                x = space; t = linspace(vec(i-1),vec(i),10);
                options = odeset('RelTol',reltol,'AbsTol',abstol,'NormControl','on');
                temp = pdepe(0,@(x,t,u,DuDx)pde(x,t,u,DuDx,var,space,DF,...
                             ConVec,PDEInit),@(x)pdeic(x,var,space,PDEInit),...
                             @(xl,ul,xr,ur,t)pdebc(xl,ul,xr,ur,t,var),x,t,options);
                for j=1:var
                    PDEInit(:,j) = temp(end,:,j);
                    Y(i,:,j)=temp(end,:,j);
                end
            end
    case 'methodoflines'
            for i=2:length(vec)
                for j=1:num
                    ConVec(:,j)=ConVect(i-1,:,j);
                end
                s = state(vec(i-1),PDEInit,space,ConVec,method);
                for j=1:var
                    ff = @(t,y) (DF(j)*A*y + s(:,j));
                    options = odeset('RelTol',reltol,'AbsTol',abstol,'MaxStep',0.1);
                    temp=feval(MatlabSolver,ff,[vec(i-1) vec(i)],PDEInit(:,j),options);
                    temp.extdata.odefun =[];
                    temp=deval(temp,vec(i));
                    PDEInit(:,j) = temp;
                end
                for j=1:var
                    Y(i,:,j)=PDEInit(:,j);
                end
            end
    otherwise 
            warning('Unexpected method')
end

% --------------------------------------------------------------
function [c,f,s] = pde(x,t,u,DuDx,var,space,DF,ConVec,PDEInit)
c = ones(var,1); 
f = DF .* DuDx; 
[s, ~] = state(x,u,space,ConVec,PDEInit,t);
% --------------------------------------------------------------
function u0 = pdeic(x,~,space,G)
for i=1:length(space)
    if x==space(i)
        u0=G(i,:);
    end
end
u0=u0';
% --------------------------------------------------------------
function [pl,ql,pr,qr] = pdebc(~,~,~,~,~,var)
pl = zeros(var,1); 
ql = ones(var,1); 
pr = zeros(var,1); 
qr = ones(var,1);