function Y=PDEForward2(ConVect,A,vec,var,num,space,DF,PDEInit,tol)
reltol=tol(1);abstol=tol(2);
Y=zeros(length(vec),length(space),var);
ConVec=zeros(length(space),num);  % ConVec = Control variable vector
Y(1,:,:)=PDEInit;
for i=2:length(vec)
    for j=1:num
        ConVec(:,j)=ConVect(i-1,:,j);
    end
    for j=1:var
        ff = @(t,y) (DF(j)*A*y + state(0,y,space,ConVec,t));
        options = odeset('RelTol',reltol,'AbsTol',abstol,'MaxStep',0.1);
        temp=feval('ode15s',ff,[vec(i-1) vec(i)],PDEInit(:,j),options);
        temp=deval(temp,vec(i));
        PDEInit(:,j) = temp;
    end
    Y(i,:,:)=PDEInit;
end





