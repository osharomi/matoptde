function dy=CoState2(x,y,space,ConVec,VarVec,var,rho,time)
dx=0.0001;
dy=zeros(size(y));
for i=1:var
    temp0 = VarVec;
    temp0(i)=VarVec(i)+dx;    
    [temp_dx1, obj1]=state(x,temp0,space,ConVec,time);
    temp = 0;
    for j=1:var
        temp = temp + y(:,j)*temp_dx1(j);
    end
    temp_main1 = obj1+temp;
    
    temp1 = VarVec;
    temp1(i)=VarVec(i)-dx;    
    [temp_dx2, obj2]=state(x,temp1,space,ConVec,time);
    tempz = 0;
    for j=1:var
        tempz = tempz + y(:,j)*temp_dx2(j);
    end
    temp_main2 = obj2+tempz;    
    
    dy(:,i) = (temp_main1-temp_main2)/(2*dx);
end
dy=dy-rho*y;