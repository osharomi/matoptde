function f=PDEHamiltonian(x,PDESoln,CPDESoln,space,var,opt,time,method,jj)
if strcmp(opt,'min')==1
    mul=1;
elseif strcmp(opt,'max')==1
    mul=-1;
end
[dy, obj]=state(time,PDESoln,space,x,method,{'con',jj});
temp = 0;
for i=1:var
    temp = temp + CPDESoln(i)*dy(i);
end
f = mul*(obj+temp);