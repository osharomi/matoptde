function PDE_main(directory)
% Initial condition, Final condition and number of variables
% numc = number of controls
numc=2;
% BOUNDARY POINTS
a = -1;
b = 1;
dx = 0.05;
% FT = Final time
FT=10;
% dt = Timestep
dt=(dx^2)/4;
% Min or Max
opt = 'max';
% rho
rho=0;
% vec = Time array
vec=0:dt:FT;
space=a:dx:b;
% number of iterations
numIter = 100;
% init = initial guess for control
ConVec = 0*ones(length(vec),length(space),numc);
% type = 'constraint' (if controls are constraint) 
% and 'unconstraint' (if controls are constraint) 
% and 'manual' (if relationship is known)
type = 'manual';
MatlabSolver='ode45';
% var = number of variables
var=2;
% DF = Diffusion coefficient
DF=[0.001; 0.001];
tol = [1e-6 1e-12];
method='methodoflines';
switch method
    case 'methodoflines'
       % sig = dt/(dx^2)
        if (dt/(dx^2)>=0.5)
            error('dt/dx^2 must be less than 0.5')
        end
end
% PDEinit is the PDE Initial condition for state variables
PDEInit=zeros(length(space),var);
PDEInit(:,1)=10*ones(size(space));
PDEInit(:,2)=exp(space);
% Coeffficient Matrix
A = laplacianMatrix(length(PDEInit(:,1)));
A = -A;
A(1,:)=2*A(1,:);A(end,:)=2*A(end,:);
A = (1/(dx^2))*A;
%-----------------------------------------
oldya = ones(length(vec),length(space),var);
oldyb = ones(length(vec),length(space),var);
errck = zeros(size(numc));
erryak = zeros(size(var));errybk = zeros(size(var));
fprintf('%5s %15s %15s %15s %15s \n','Iteration','ControlError',...
                                'StateError','CoStateError','MaxError')
for i=1:numIter
    oldinit=ConVec;
    PDESoln=PDEForward(ConVec,dt,vec,var,numc,space,DF,PDEInit,tol,method,MatlabSolver);
    %  PDEFinal is the PDE Final condition for co-state variables
    PDEFinal=zeros(length(space),var);
    %---------------------------------------------
    CPDESoln=PDEBackward(ConVec,PDESoln,dt,vec,var,numc,space,DF,PDEFinal,rho,tol,method,MatlabSolver); 
    %  Optimization routine
    ConVec=PDEControlOpt(oldinit,PDESoln,CPDESoln,vec,var,numc,space,type,opt);
    %  Error control
    for kk = 1:numc
    errck(kk)=norm(ConVec(:,:,kk)-oldinit(:,:,kk))/norm(ConVec(:,:,kk));
    end
    for kk = 1:var
    erryak(kk)=norm(PDESoln(:,:,kk)-oldya(:,:,kk))/norm(PDESoln(:,:,kk));
    errybk(kk)=norm(CPDESoln(:,:,kk)-oldyb(:,:,kk))/norm(CPDESoln(:,:,kk));
    end
    errc = max(errck);errya = max(erryak);erryb = max(errybk);
    oldya=PDESoln;oldyb=CPDESoln;
    err=max([errc errya erryb]);
    fprintf('%5d %18.2e %16.2e %14.2e %16.2e \n',i,errc,errya,erryb,err);
    if (err<1e-6)
        break
    end
    plotfunction
end
directory = strcat(directory,'solution.mat');
save(directory,'ConVec','vec','PDESoln','CPDESoln','space','vec')

%u1=PDESoln(:,:,1);
%surf(space,vec,u1)
%plot(vec,ConVec,'color','blue');
%subplot(1,2,1), surf(space,vec,ConVec(:,:,1)); axis tight;
%subplot(1,2,2), surf(space,vec,ConVec(:,:,2));  axis tight;
