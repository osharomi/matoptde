%load('solution.mat')

u1=PDESoln(:,:,1);u2=PDESoln(:,:,2);
u3=ConVec(:,:,1);u4=ConVec(:,:,2);
subplot(2,2,1);h1=surf(space,vec,u1);set(h1, 'edgecolor','none')
axis tight;drawnow;
subplot(2,2,2);h2=surf(space,vec,u2);set(h2, 'edgecolor','none')
axis tight;drawnow;
subplot(2,2,4);h3=surf(space,vec,u3);set(h3, 'edgecolor','none')
axis tight;drawnow;
subplot(2,2,3);h4=surf(space,vec,u4);set(h4, 'edgecolor','none')
axis tight;drawnow;
