function ConVec=PDEControlOpt(init,PDESoln,CPDESoln,vec,var,num,space,type,opt)
initz=zeros(length(space),num);
PDEinit=zeros(length(space),var);
CPDEinit=zeros(length(space),var);
ConVec = zeros(length(vec),length(space),num);
if strcmp(type,'constraint')==1
    A = [1 0; 0 1; 1 1; -1 -1];
    b = [1; 1; 0.8; 0];
    lb = zeros(2,1);
    ub = ones(2,1);
    options = optimset('Display','off','Algorithm','active-set');
    for i=1:length(vec)
        for j=1:num
            initz(:,j)=init(i,:,j);
        end
        for j=1:var
            PDEinit(:,j)=PDESoln(i,:,j);
            CPDEinit(:,j)=CPDESoln(i,:,j);
        end
        for jj=1:length(space)
            y = fmincon(@(x)PDEHamiltonian(x,PDEinit(jj,:),CPDEinit(jj,:),...
                space,var,opt,PDEinit,space(jj)),initz(jj,:),A,b,[],[],lb,ub,[],options);
            ConVec(i,jj,:)= y;
        end
    end
elseif strcmp(type,'unconstraint')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(vec)
        for j=1:num
            initz(:,j)=init(i,:,j);
        end
        for j=1:var
            PDEinit(:,j)=PDESoln(i,:,j);
            CPDEinit(:,j)=CPDESoln(i,:,j);
        end
        for jj=1:length(space)
            y = fminunc(@(x)PDEHamiltonian(x,PDEinit(jj,:),CPDEinit(jj,:),...
                space,var,opt,PDEinit,space(jj)),initz(jj,:),options);
            ConVec(i,jj,:)=y;
        end
    end
elseif strcmp(type,'manual')==1
    eta=0.2;
    ConVec(:,:,1)=1 + (eta*CPDESoln(:,:,1)./(2*PDESoln(:,:,1)));
    %ConVec=PDEControl(PDESoln, CPDESoln);
end