function [dy, obj]=state(time,y,space,ConVec,method)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 
switch method
    case 'matlabpdepe'
        if strcmp(xx(1),'str')~=1
            x = xx(1);
            tau=interp1(space,ConVec(:,1),x);
            c=interp1(space,ConVec(:,2),x);
        else
            x = xx{2};
            tau=ConVec(1);
            c=ConVec(2);
        end

        sigma=2;thetat=2;thetap=0.001;
        alpha1=1;alpha2=0.1;deltap=0.4;
        deltak=0.02;gamma=0.3;theta=1;
        n=2;
        f = (alpha1*y(1)^n)/(1+alpha2*y(1)^n);
        dy=[(f*(1-tau-c))/(1+thetap*y(2)) - deltak*y(1)
            (sigma*f)/(1+thetat*tau) - deltap*y(2)];

        obj = theta*c*f - gamma*y(2);
    case 'methodoflines'
        tau=ConVec(:,1);
        c=ConVec(:,2);
        sigma=2;thetat=2;thetap=0.001;
        alpha1=1;alpha2=0.1;deltap=0.4;
        deltak=0.02;gamma=0.3;theta=1;
        n=2;
        f = (alpha1*y(:,1).^n)./(1+alpha2*y(:,1).^n);
        dy(:,1)=(f.*(1-tau-c))./(1+thetap*y(:,2)) - deltak*y(:,1);
        dy(:,2)=(sigma*f)./(1+thetat*tau) - deltap*y(2);

        obj = (theta*c.*f - gamma*y(:,2)); 
    otherwise
        warning('Unexpected method')
end