function ConVec=PDEControlOpt(init,PDESoln,CPDESoln,vec,var,num,space,type,opt)
initz=zeros(length(space),num);
PDEinit=zeros(length(space),var);
CPDEinit=zeros(length(space),var);
ConVec = zeros(length(vec),length(space),num);
if strcmp(type,'constraint')==1
    A = [-1; -1];
    b = [0; 0];
    options = optimset('Display','off','Algorithm','active-set','TolFun',1e-12);
    for i=1:length(vec)
        for j=1:num
            initz(:,j)=init(i,:,j);
        end
        for j=1:var
            PDEinit(:,j)=PDESoln(i,:,j);
            CPDEinit(:,j)=CPDESoln(i,:,j);
        end
        time = vec(i);
        for jj=1:length(space)
            y = fmincon(@(x)PDEHamiltonian(x,PDEinit(jj,:),CPDEinit(jj,:),...
                space,var,opt,PDEinit,space(jj),time),initz(jj,:),A,b,[],[],[],[],[],options);
            ConVec(i,jj,:)= y;
        end
    end
elseif strcmp(type,'unconstraint')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(vec)
        for j=1:num
            initz(:,j)=init(i,:,j);
        end
        for j=1:var
            PDEinit(:,j)=PDESoln(i,:,j);
            CPDEinit(:,j)=CPDESoln(i,:,j);
        end
        time = vec(i);
        for jj=1:length(space)
            y = fminunc(@(x)PDEHamiltonian(x,PDEinit(jj,:),CPDEinit(jj,:),...
                space,var,opt,PDEinit,space(jj),time),initz(jj,:),options);
            ConVec(i,jj,:)=y;
        end
    end
elseif strcmp(type,'manual')==1
    for i=1:length(vec)
        for j=1:length(space)
            ConVec(i,j,1)=(pi^2)*cos(pi*space(j))*exp(vec(i))-CPDESoln(i,j,1)/2; %
        end
    end
end