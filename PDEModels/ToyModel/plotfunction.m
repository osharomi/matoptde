function plotfunction
load('solution.mat')
mm =20000;
nn = length(1:mm:length(vec));
vecn = 1:mm:length(vec);
for i=1:nn
    xcv(i) = vec(vecn(i));
end

u1=PDESoln(1:mm:length(vec),:,1);
u2=ConVec(1:mm:length(vec),:,1);
subplot(2,2,1);surf(space,xcv,u1,'LineStyle','none');axis tight;drawnow;
subplot(2,2,2);surf(space,xcv,u2,'LineStyle','none');axis tight;drawnow;
numsol = zeros(nn,length(space));
numcon = zeros(nn,length(space));


for i=1:nn
    xcv(i) = vec(vecn(i));
    for j=1:length(space)
        numsol(i,j)=cos(pi*space(j))*exp(vec(vecn(i)));
        numcon(i,j)=(pi^2)*cos(pi*space(j))*exp(vec(vecn(i)));
    end
end

%size(numsol)
subplot(2,2,3);surf(space,xcv,numsol,'LineStyle','none');axis tight;drawnow;
subplot(2,2,4);surf(space,xcv,(u1-numsol),'LineStyle','none');axis tight;drawnow;

% u1b=PDESoln(10,:,1);
% u2b=ConVec(500,:,1);
% subplot(1,2,1);plot(space,u1b);axis tight; drawnow; %hold on
% subplot(1,2,2);plot(space,u2b);axis tight; drawnow; %hold on
% 
% u1c=PDESoln(50,:,1);
% subplot(1,2,1);plot(space,u1c);axis tight; drawnow; %hold on
% 
% u1d=PDESoln(150,:,1);
% subplot(1,2,1);plot(space,u1d);axis tight; drawnow; %hold on