function PDE_main(directory)
% Initial condition, Final condition and number of variables
% numc = number of controls
numc=1;
% FT = Final time
FT=1;
% BOUNDARY POINTS
a = 0;
b = 1;
dx = 0.01;
% dt = Timestep
dt=(dx^2)/4;
% Min or Max
opt = 'min';
% rho
rho=0;
% vec = Time array
vec=0:dt:FT;
space = a:dx:b;
% number of iterations
numIter = 10;
% init = initial guess for control
ConVec =ones(length(vec),length(space),numc);
% for i=1:length(vec)
%     for j=1:length(space)
%     ConVec(i,j,1)=(pi^2)*cos(pi*space(j))*exp(vec(i));
%     end
% end
% type = 'constraint' (if controls are constraint) 
% and 'unconstraint' (if controls are constraint) 
% and 'constraint' (if relationship is known)
type = 'manual';
MatlabSolver='ode45';
% var = number of variables
var=1;
% DF = Diffusion coefficient
DF=1;
tol = [1e-12 1e-15];
method='methodoflines';
switch method
    case 'methodoflines'
       % sig = dt/(dx^2)
        if (dt/(dx^2)>=0.5)
            error('dt/dx^2 must be less than 0.5')
        end
end
% PDEinit is the PDE Initial condition for state variables
PDEInit=ones(length(space),var);
for i=1:length(space)
    PDEInit(i,1)=cos(pi*space(i));
end
% Coeffficient Matrix
A = laplacianMatrix(length(PDEInit(:,1)));
A = -A;A(1,:)=2*A(1,:);A(end,:)=2*A(end,:);
A = (1/(dx^2))*A;
%-----------------------------------------
oldya = ones(length(vec),length(space),var);
oldyb = ones(length(vec),length(space),var);
errck = zeros(size(numc));
erryak = zeros(size(var));
errybk = zeros(size(var));
fprintf('%5s %15s %15s %15s %15s %15s \n','Iteration','ControlError',...
                                'StateError','CoStateError','MaxError', 'IterationTime')
for i=1:numIter
    tic
    oldinit=ConVec;
    PDESoln=PDEForward(ConVec,A,vec,var,numc,space,DF,PDEInit,tol,method,MatlabSolver);
    %  PDEFinal is the PDE Final condition for co-state variables
    PDEFinal=zeros(length(space),var);
    %---------------------------------------------
    CPDESoln=PDEBackward(ConVec,PDESoln,A,vec,var,numc,space,DF,PDEFinal,rho,tol,method,MatlabSolver);
    %  Optimization routine
    ConVec=PDEControlOpt(oldinit,PDESoln,CPDESoln,vec,var,numc,space,type,opt);
    spent=toc;
    %  Error control
    for kk = 1:numc
    errck(kk)=norm(ConVec(:,:,kk)-oldinit(:,:,kk))/norm(ConVec(:,:,kk));
    end
    for kk = 1:var
    erryak(kk)=norm(PDESoln(:,:,kk)-oldya(:,:,kk))/norm(PDESoln(:,:,kk));
    errybk(kk)=norm(CPDESoln(:,:,kk)-oldyb(:,:,kk))/norm(CPDESoln(:,:,kk));
    end
    errc = max(errck);errya = max(erryak);erryb = max(errybk);
    oldya=PDESoln;oldyb=CPDESoln;
    err=max([errc errya erryb]);
    fprintf('%5d %18.2e %16.2e %14.2e %16.2e %10g \n',i,errc,errya,erryb,err,spent);
    if (errc<1e-6)
        break
    end
   % plotfunction
end
directory = strcat(directory,'solution.mat');
save(directory,'ConVec','vec','PDESoln','CPDESoln','space','vec','-v7.3');
