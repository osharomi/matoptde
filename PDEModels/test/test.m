function test
m = 0;
a = -1;b = 1;dx = 0.1;x=a:dx:b;
FT=10;dt=0.1;t=0:dt:FT;


sol = pdepe(m,@pdex4pde,@pdex4ic,@pdex4bc,x,t);
u1 = sol(:,:,1);
u2 = sol(:,:,2);

subplot(1,2,1); surf(x,t,u1)
title('u1(x,t)')
xlabel('Distance x')
ylabel('Time t')

subplot(1,2,2); surf(x,t,u2)
title('u2(x,t)')
xlabel('Distance x')
ylabel('Time t')
% --------------------------------------------------------------
function [c,f,s] = pdex4pde(x,t,u,DuDx)
c = [1; 1]; 
f = [0.001; 0.001] .* DuDx; 
sigma=2;thetat=2;thetap=0.001;
alpha1=1;alpha2=0.1;deltap=0.4;
deltak=0.02;n=2;tau=0;cc=0;
ff = (alpha1*u(1)^n)/(1+alpha2*u(1)^n);
s = [(ff*(1-tau-cc))/(1+thetap*u(2)) - deltak*u(1); (sigma*ff)/(1+thetat*tau) - deltap*u(2)]; 
% --------------------------------------------------------------
function u0 = pdex4ic(x)
u0 = [10; exp(x)]; 
% --------------------------------------------------------------
function [pl,ql,pr,qr] = pdex4bc(xl,ul,xr,ur,t)
pl = [0; 0]; 
ql = [1; 1]; 
pr = [0; 0]; 
qr = [1; 1]; 