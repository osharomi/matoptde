function [c, ceq] = confun(x,PDESoln,space,xx)
% Nonlinear inequality constraints
phi=0.5;a=0.25;
alpha=1/3;
rho=0.03;psi = a*exp(-phi*abs(space));

c = [x(1) - 10*(PDESoln(1)^alpha)
     -a*exp(-phi*abs(xx))
     phi*phi*a*exp(-phi*abs(xx))-rho*a*exp(-phi*abs(xx))];
% Nonlinear equality constraints
ceq = [1-trapz(space,psi)];