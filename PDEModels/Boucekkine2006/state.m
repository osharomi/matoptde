function [dy, obj]=state(xx,y,space,ConVec,PDESoln)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 

if strcmp(xx(1),'str')~=1
    x = xx(1);
    c=interp1(space,ConVec(:,1),x);
else
    x = xx{2};
    c=ConVec(1);
end
A=10;delta=0.3;phi=0.5;a=0.25;alpha=1/3;
psi = a*exp(-phi*abs(x));

dy=[A*(y(1)^alpha)-c-delta*y(1)];
obj = c*psi;