function [dy, obj]=state(time,y,space,ConVec,method)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 
switch method
    case 'matlabpdepe'
        if strcmp(xx(1),'str')~=1
            x = xx(1);
            c=interp1(space,ConVec(:,1)',x,'spline');
            %c = (pi^2)*cos(pi*x)*exp(time);
        else
            x = xx{2};
            c=ConVec(1);
        end
        dy = y(1) + 2*(pi^2)*y(1) - c;
        obj = (c - (pi^2)*cos(pi*x)*exp(time))^2;
    case 'methodoflines'
        c = ConVec(:,1);
        dy = y(:,1) + 2*(pi^2)*y(:,1) - c;
        obj = (c - (pi^2)*cos(pi*space')*exp(time)).^2;
    otherwise
        warning('Unexpected method')
end