%load('solution.mat')

mm =5000;
nn = length(1:mm:length(vec));
u1=PDESoln(1:mm:length(vec),:,1);
u2=ConVec(1:mm:length(vec),:,1);
subplot(2,2,1);surf(space,1:mm:length(vec),u1,'LineStyle','none');axis tight;drawnow;
subplot(2,2,2);surf(space,1:mm:length(vec),u2,'LineStyle','none');axis tight;drawnow;
numsol = zeros(nn,length(space));
numcon = zeros(nn,length(space));
vecn = 1:mm:length(vec);
for i=1:nn
    for j=1:length(space)
        numsol(i,j)=cos(pi*space(j))*exp(vec(vecn(i)));
        numcon(i,j)=(pi^2)*cos(pi*space(j))*exp(vec(vecn(i)));
    end
end
%size(numsol)
subplot(2,2,3);surf(space,1:mm:length(vec),numsol,'LineStyle','none');axis tight;drawnow;
subplot(2,2,4);surf(space,1:mm:length(vec),(u1-numsol),'LineStyle','none');axis tight;drawnow;

