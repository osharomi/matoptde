function ConVec=PDEControlOpt(init,PDESoln,CPDESoln,vec,var,num,space,type,opt,method)
initz=zeros(length(space),num);
PDEinit=zeros(length(space),var);
CPDEinit=zeros(length(space),var);
ConVec = zeros(length(vec),length(space),num);
if strcmp(type,'constraint')==1
    A = [1; -1];
    b = [1; 0];
    lb = zeros(1,1);
    ub = ones(1,1);
    options = optimset('Display','off','Algorithm','active-set','TolFun',1e-12);
    for i=1:length(vec)
        for j=1:num
            initz(:,j)=init(i,:,j);
        end
        for j=1:var
            PDEinit(:,j)=PDESoln(i,:,j);
            CPDEinit(:,j)=CPDESoln(i,:,j);
        end
        for jj=1:length(space)
            y = fmincon(@(x)PDEHamiltonian(x,PDEinit(jj,:),CPDEinit(jj,:),...
                space,var,opt,vec(i),method,jj),initz(jj,:),A,b,[],[],lb,ub,[],options);
            ConVec(i,jj,:)= y;
        end
    end
elseif strcmp(type,'unconstraint')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(vec)
        for j=1:num
            initz(:,j)=init(i,:,j);
        end
        for j=1:var
            PDEinit(:,j)=PDESoln(i,:,j);
            CPDEinit(:,j)=CPDESoln(i,:,j);
        end
        for jj=1:length(space)
            y = fminunc(@(x)PDEHamiltonian(x,PDEinit(jj,:),CPDEinit(jj,:),...
                space,var,opt,PDEinit,space(jj)),initz(jj,:),options);
            ConVec(i,jj,:)=y;
        end
    end
elseif strcmp(type,'manual')==1
    for ii = 1:length(vec)
        for jj=1:length(space)
            hub = 1-CPDESoln(ii,jj,1);%
            if hub < 0
                ConVec(ii,jj,1)= 0;
            elseif hub > 0
                ConVec(ii,jj,1) = 1;
            end
        end
    end
end