function [dy, obj]=state(time,y,space,ConVec,method)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 
delta1=0.05;delta2=0.01;s=0.25;alpha1=100;
alpha2=100;gamma=4;epsilon=0.001;theta=0.1;rho=0.1;
switch method
    case 'matlabpdepe'
         if strcmp(xx(1),'str')~=1
            x = xx(1);
            c=interp1(space,ConVec(:,1),x,'pchip');
         else
            x = xx{2};
            c=ConVec(1);
         end
         F2=(1/(sqrt(pi*epsilon)))*(exp(-(((abs(x-space)).^2)/epsilon)))*(x^2);
         zz=((alpha1*(y(:,1).^gamma))./(1+alpha2*(y(:,1).^gamma))).*F2';
         f = (alpha1*(y(1)^gamma))/(1+alpha2*(y(1)^gamma));
         dy=[((s*f)/(1+y(2)^2)) - delta1*y(1) - c*y(1)
             theta*trapz(space,zz) - delta2*y(2)];
         obj = -c*y(1);
    case 'methodoflines'
         c=ConVec(:,1);
         if length(y(:,1))<=75
             zc = ones(size(length(y(:,1))));
             for i=1:length(space)
                 xp=space(i);
                 ff=@(xv) (((alpha1*(y(i,1)^gamma))./(1+alpha2*(y(i,1).^gamma)))...
                     *((1/(sqrt(pi*epsilon)))*(exp(-(((abs(xp-xv)).^2)/epsilon)))*(xp^2)));
                 zc(i,1)=integral(ff,space(1),space(end));
             end
         else
             ff=@(xv) ((alpha1*(y(:,1).^gamma))./(1+alpha2*(y(:,1).^gamma))).*...
                  ((1/(sqrt(pi*epsilon)))).*(exp(-(((abs(space'-xv)).^2)/epsilon))).*((space').^2); %...            
             zc=integral(ff,space(1),space(end),'ArrayValued',true);
         end
         f = (alpha1*(y(:,1).^gamma))./(1+(alpha2*(y(:,1).^gamma)));
         dy(:,1)=((s*f)./(1+y(:,2).^2))  - delta1*y(:,1) - c.*y(:,1);
         dy(:,2)=theta*zc - delta2*y(:,2);
         obj = c.*y(:,1); 
    otherwise
        warning('Unexpected method')
end