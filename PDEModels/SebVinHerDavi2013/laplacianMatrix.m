function a = laplacianMatrix(n)
  a = toeplitz([2,-1,zeros(1,n-2)]);
  a([1,end]) = 1;
end