%load('solution.mat')

u1=PDESoln(:,:,1);u2=PDESoln(:,:,2);
u3=ConVec(:,:,1);%u4=ConVec(:,:,2);
subplot(2,2,1);h1=surf(space,vec,u1);axis tight;drawnow;
set(h1,'LineStyle','none')
subplot(2,2,2);h2=surf(space,vec,u2);axis tight;drawnow;
set(h2,'LineStyle','none')
subplot(2,2,3);h3=surf(space,vec,u3);axis tight;drawnow;
set(h3,'LineStyle','none')