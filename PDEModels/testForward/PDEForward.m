function Y=PDEForward(ConVect,dt,vec,var,num,space,DF,PDEInit,tol)
% -------------------------------
reltol=tol(1);abstol=tol(2);
Y=zeros(length(vec),length(space),var);
ConVec=zeros(num,length(space));  % ConVec = Control variable vector
Y(1,:,:)=PDEInit';
for i=2:length(vec)
    for j=1:num
        ConVec(j,:)=ConVect(i,:,j);
    end
    x = space; t = linspace(0,dt,10);
    options = odeset('RelTol',reltol,'AbsTol',abstol);
    temp = pdepe(0,@(x,t,u,DuDx)pde(x,t,u,DuDx,var,space,DF,ConVec,PDEInit',vec(i))...
          ,@(x)pdeic(x,var,space,PDEInit), @(xl,ul,xr,ur,t)pdebc(xl,ul,xr,ur,t,var)...
          ,x,t,options);
    for j=1:var
        PDEInit(j,:) = temp(end,:,j);
    end
    for j=1:var
        Y(i,:,j)=temp(end,:,j);
    end
end

% --------------------------------------------------------------
function [c,f,s] = pde(x,~,u,DuDx,var,space,DF,ConVec,PDEInit,time)
c = ones(1,var)'; 
f = DF .* DuDx; 
s = state(x,u,space,ConVec,PDEInit,time);
% --------------------------------------------------------------
function u0 = pdeic(x,~,space,G)
for i=1:length(space)
    if x==space(i)
        u0=G(:,i);
    end
end
% --------------------------------------------------------------
function [pl,ql,pr,qr] = pdebc(~,~,~,~,~,var)
pl = zeros(1,var)'; 
ql = ones(1,var)'; 
pr = zeros(1,var)'; 
qr = ones(1,var)';