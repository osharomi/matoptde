function ConVec=PDEControlOpt(init,PDESoln,CPDESoln,vec,var,num,space,type,opt,ConVecz)

if strcmp(type,'constraint')==1
    A = [1; -1];
    b = [300; 0];
    lb = zeros(1,1);
    ub = 300*ones(1,1);
    options = optimset('Display','off','Algorithm','active-set');
    for i=1:length(vec)
        for j=1:num
            initz(j,:)=init(i,:,j);
        end
        for j=1:var
            PDEinit(j,:)=PDESoln(i,:,j);
            CPDEinit(j,:)=CPDESoln(i,:,j);
        end
        for jj=1:length(space)
            x = fmincon(@(x)PDEHamiltonian(x,PDEinit(:,jj),CPDEinit(:,jj),...
                space,var,opt,PDEinit',space(jj)),initz(:,jj),A,b,[],[],lb,ub,[],options);
            ConVec(i,jj,:)=x;
        end
    end
elseif strcmp(type,'unconstraint')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(vec)
        for j=1:num
            initz(j,:)=init(i,:,j);
        end
        for j=1:var
            PDEinit(j,:)=PDESoln(i,:,j);
            CPDEinit(j,:)=CPDESoln(i,:,j);
        end
        for jj=1:length(space)
            x = fminunc(@(x)PDEHamiltonian(x,PDEinit(:,jj),CPDEinit(:,jj),...
                space,var,opt,PDEinit',space(jj)),initz(:,jj),options);
            ConVec(i,jj,:)=x;
        end
    end
elseif strcmp(type,'manual')==1
    eta=0.2;
    ConVec=1 + (eta*CPDESoln(:,:,1)./(2*PDESoln(:,:,1)));
    %ConVec=PDEControl(PDESoln, CPDESoln);
end