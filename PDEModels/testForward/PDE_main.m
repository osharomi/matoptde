function PDE_main()
% Initial condition, Final condition and number of variables
% numc = number of controls
numc=1;
% FT = Final time
FT=2;
% dt = Timestep
dt=0.1;
% Min or Max
opt = 'max';
% rho
rho=0.1;
% BOUNDARY POINTS
a = 0;
b = 1;
dx = 0.1;
% vec = Time array
vec=0:dt:FT;
space=a:dx:b;
% number of iterations
numIter = 10;
% init = initial guess for control
ConVec = ones(length(vec),length(space),numc);
% type = 'constraint' (if controls are constraint) 
% and 'unconstraint' (if controls are constraint) 
% and 'manual' (if relationship is known)
type = 'manual';
% var = number of variables
var=2;
% DF = Diffusion coefficient
DF=[0.024; 0.17];
tol = [1e-12 1e-12];
% PDEinit is the PDE Initial condition for state variables
PDEInit=ones(var,length(space));
PDEInit(2,:)=0;
% PDEInit=exp((-space.^2)/0.1);
%-----------------------------------------
% oldya = ones(length(vec),length(space),var);
% oldyb = ones(length(vec),length(space),var);
% fprintf('%5s %15s %15s %15s %15s \n','Iteration','ControlError',...
%                                 'StateError','CoStateError','MaxError')
%for i=1:numIter
%    oldinit=ConVec;
    PDESoln=PDEForward(ConVec,dt,vec,var,numc,space,DF,PDEInit,tol);
    %  PDEFinal is the PDE Final condition for co-state variables
    %  PDEFinal=ones(var,length(space));
%     PDEF=PDESoln(end,:,:);theta=0.5;
%     PDEFinal=((1-theta)/theta)*PDEF;  
%     %---------------------------------------------
%     CPDESoln=PDEBackward(ConVec,PDESoln,dt,vec,var,numc,space,DF,PDEFinal,rho,tol); 
%     %  Optimization routine
%     ConVec=PDEControlOpt(oldinit,PDESoln,CPDESoln,vec,var,numc,space,type,opt,ConVec);
%     %  Error control
%     for kk = 1:numc
%     errck(kk)=norm(ConVec(:,:,kk)-oldinit(:,:,kk))/norm(ConVec(:,:,kk));
%     end
%     for kk = 1:var
%     erryak(kk)=norm(PDESoln(:,:,kk)-oldya(:,:,kk))/norm(PDESoln(:,:,kk));
%     errybk(kk)=norm(CPDESoln(:,:,kk)-oldyb(:,:,kk))/norm(CPDESoln(:,:,kk));
%     end
%     errc = max(errck);errya = max(erryak);erryb = max(errybk);
%     oldya=PDESoln;oldyb=CPDESoln;
%     err=max([errc errya erryb]);
%     fprintf('%5d %18.2e %16.2e %14.2e %16.2e \n',i,errc,errya,erryb,err);
%     if (err<1e-6)
%         break
%     end
%end
directory = strcat('solution.mat');
save(directory,'ConVec','vec','PDESoln','space')
%u1=PDESoln(:,:,1);
%surf(space,vec,u1)
%plot(vec,ConVec,'color','blue');
%subplot(1,2,1), plot(Storage(1,:));axis tight;
%subplot(1,2,2), plot(Storage(2,:),'Color',[1 0 0]); axis tight;
