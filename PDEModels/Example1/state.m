function [dy, obj]=state(xx,y,space,ConVec,PDESoln)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 
if strcmp(xx(1),'str')~=1
    x = xx(1);
    tau=interp1(space,ConVec(:,1),x,'spline');
else
    x = xx{2};
    tau=ConVec(1);
end

eta=0.2;
delta=0.1;
dy=eta*(1-tau-delta)*y(1);

obj = (y(1))^2*(1-tau)^2;