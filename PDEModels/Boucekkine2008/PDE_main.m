function PDE_main(directory)
% Initial condition, Final condition and number of variables
% numc = number of controls
numc=1;
% FT = Final time
FT=500;
% dt = Timestep
dt=100;
% Min or Max
opt = 'max';
% rho
rho=0.03;
% BOUNDARY POINTS
a = 0;
b = 1000;
dx = 1;
% vec = Time array
vec=0:dt:FT;
space=a:dx:b;
% number of iterations
numIter = 100;
% init = initial guess for control
ConVec =ones(length(vec),length(space),numc);
% type = 'constraint' (if controls are constraint) 
% and 'unconstraint' (if controls are constraint) 
% and 'manual' (if relationship is known)
type = 'manual';
% var = number of variables
var=1;
% DF = Diffusion coefficient
DF=1;
tol = [1e-12 1e-12];
% PDEinit is the PDE Initial condition for state variables
PDEInit=ones(length(space),var);
%PDEInit(:,1)=100+50*exp(-((space-500).^2)/(2*100^2));
for i=1:length(space)
    if space(i)<=500
        PDEInit(i,1)=33*exp((space(i)-500)/100);
    else
        PDEInit(i,1)=33*(space(i)-500).^0.3;
    end
end
% sigma=10;phi=0.001;
%-----------------------------------------
oldya = ones(length(vec),length(space),var);
oldyb = ones(length(vec),length(space),var);
errck = zeros(size(numc));
erryak = zeros(size(var));errybk = zeros(size(var));
fprintf('%5s %15s %15s %15s %15s %15s \n','Iteration','ControlError',...
                                'StateError','CoStateError','MaxError', 'IterationTime')
for i=1:numIter
    tic
    oldinit=ConVec;
    PDESoln=PDEForward(ConVec,dt,vec,var,numc,space,DF,PDEInit,tol);
    %  PDEFinal is the PDE Final condition for co-state variables
    PDEFinal=ones(length(space),var);
%     for ii=1:length(space)
%         if space(ii)<=500
%             PDEFinal(ii,1)=1.54 + (1.6e-4)*space(ii);
%         else
%             PDEFinal(ii,1)=1.7 - (1.6e-4)*space(ii);
%         end
%     end
    %PDEFinal=((exp(-phi*abs(space)))'.*(PDEFinal.^(-sigma)))/(sigma*(sigma-1));
    %PDEFinal=((1-theta)/theta)*PDEF;  
    %---------------------------------------------
    CPDESoln=PDEBackward(ConVec,PDESoln,dt,vec,var,numc,space,DF,PDEFinal,rho,tol); 
    %  Optimization routine
    ConVec=PDEControlOpt(oldinit,PDESoln,CPDESoln,vec,var,numc,space,type,opt);
    spent=toc;
    %  Error control
    for kk = 1:numc
    errck(kk)=norm(ConVec(:,:,kk)-oldinit(:,:,kk))/norm(ConVec(:,:,kk));
    end
    for kk = 1:var
    erryak(kk)=norm(PDESoln(:,:,kk)-oldya(:,:,kk))/norm(PDESoln(:,:,kk));
    errybk(kk)=norm(CPDESoln(:,:,kk)-oldyb(:,:,kk))/norm(CPDESoln(:,:,kk));
    end
    errc = max(errck);errya = max(erryak);erryb = max(errybk);
    oldya=PDESoln;oldyb=CPDESoln;
    err=max([errc errya erryb]);
    fprintf('%5d %18.2e %16.2e %14.2e %16.2e %10g \n',i,errc,errya,erryb,err,spent);
    if (err<1e-6)
        break
    end
    plotfunction
end
directory = strcat(directory,'solution.mat');
save(directory,'ConVec','vec','PDESoln','space','vec')
