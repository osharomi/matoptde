function [dy, obj]=state(xx,y,space,ConVec,PDESoln)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 

if strcmp(xx(1),'str')~=1
    x = xx(1);
    c=interp1(space,ConVec(:,1),x);
else
    x = xx{2};
    c=ConVec(1);
end
delta=0.06;phi=0.001;sigma=10;alpha=1/3;
A=10; %+100*exp(-((x-500)^2)/(2*100^2));
dy=A*(y(1)^alpha)-c-delta*y(1);
obj = ((c^(1-sigma))/(1-sigma))*exp(-phi*abs(x));