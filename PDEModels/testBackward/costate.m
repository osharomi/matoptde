function [dy, obj]=costate(xx,y,space,ConVec,PDESoln)
% pdepe uses finite element and tau needs to be provided at off-grid points
% interp1 is used 
if strcmp(xx(1),'str')~=1
    x = xx(1);
    tau=interp1(space,ConVec(1,:),x,'spline');
else
    x = xx{2};
    tau=ConVec(1);
end

z = y(1) - y(2);
F = exp(5.73*z)-exp(-11.47*z);
dy=[-F
    F];

obj = (y(1))^2*(1-tau)^2;