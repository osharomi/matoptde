function Y=PDEBackward(ConVect,dt,vec,var,num,space,DF,PDEFinal,rho,tol)
%---------------------------------
reltol=tol(1);abstol=tol(2);
Y=zeros(length(vec),length(space),var);
ConVec=zeros(num,length(space));  % ConVec = Control variable vector
Y(1,:,:)=PDEFinal';
for i=2:length(vec)
    kk=i;%length(vec)+1-i;
    for j=1:num
        ConVec(j,:)=ConVect(kk,:,j);
    end
    x = space; t = linspace(0,dt,100);
    options = odeset('BDF ','on','RelTol',reltol,'AbsTol',abstol,'NormControl','on');
    temp = pdepe(0,@(x,t,u,DuDx)pde(x,t,u,DuDx,var,space,DF,ConVec,PDEFinal')...
          ,@(x)pdeic(x,var,space,PDEFinal), @(xl,ul,xr,ur,t)pdebc(xl,ul,xr,ur,t,var)...
          ,x,t,options);
    for j=1:var
        PDEFinal(j,:) = temp(end,:,j);
        Y(kk,:,j)=temp(end,:,j);
    end
    
end


% --------------------------------------------------------------
function [c,f,s] = pde(x,~,u,DuDx,var,space,DF,ConVec,PDEFinal)
c = ones(1,var)'; 
f = DF .* DuDx; 
s = costate(x,u,space,ConVec,PDEFinal);
% --------------------------------------------------------------
function u0 = pdeic(x,~,space,G)
for i=1:length(space)
    if x==space(i)
        u0=G(:,i);
    end
end
% --------------------------------------------------------------
function [pl,ql,pr,qr] = pdebc(~,~,~,~,~,var)
pl = zeros(1,var)'; 
ql = ones(1,var)'; 
pr = zeros(1,var)'; 
qr = ones(1,var)';