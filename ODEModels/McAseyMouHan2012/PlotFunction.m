%load('solution.mat')

subplot(3,1,1);
plot(vec,ConVec(1,:),'color','red'); hold on
ezplot('sinh(sqrt(2)*(x-1))/(sqrt(2)*cosh(sqrt(2))+sinh(sqrt(2)))',[0 1])
ylabel('u(t)')
xlabel('t')

subplot(3,1,2);
plot(vec,ODESoln(1,:),'color','red'); hold on
ezplot('(sqrt(2)*cosh(sqrt(2)*(x-1))-sinh(sqrt(2)*(x-1)))/(sqrt(2)*cosh(sqrt(2))+sinh(sqrt(2)))',[0 1])
ylabel('x(t)')
xlabel('t')

subplot(3,1,3);
plot(vec,CODESoln(1,:),'color','red'); hold on
ezplot('-sinh(sqrt(2)*(x-1))/(sqrt(2)*cosh(sqrt(2))+sinh(sqrt(2)))',[0 1])
ylabel('\lambda(t)')
xlabel('t')

%plot(vec,ConVec(1,:),'color','blue');axis tight; drawnow;
%subplot(2,2,2); plot(vec,ConVec(2,:),'color','red');axis tight; drawnow;

% clear;clc
% load('solution.mat')
% subplot(2,2,1); 
% plot(vec,(ODESoln(4,:)+ODESoln(5,:))./(ODESoln(1,:)+ODESoln(2,:)+ODESoln(3,:)+ODESoln(4,:)+ODESoln(5,:)+ODESoln(6,:)),'color','blue');axis([0 5 0 0.16]); hold on drawnow;
% clear;clc
% load('solution_nocontrol.mat')
% subplot(2,2,2); 
% plot(vec,(ODESoln(4,:)+ODESoln(5,:))./(ODESoln(1,:)+ODESoln(2,:)+ODESoln(3,:)+ODESoln(4,:)+ODESoln(5,:)+ODESoln(6,:)),'color','blue');axis([0 5 0 0.16]); drawnow;
%subplot(2,2,2); plot(vec,ConVec(2,:),'color','red');axis tight; drawnow;
