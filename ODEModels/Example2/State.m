function [dy, obj]=State(t,y,ConVec)
u1=ConVec(1);
u2=ConVec(2);
G1=1;G2=1;G3=1;G4=1;G5=1;G6=1;G7=50;G8=500;

taum=0.3;tauf=0.3;rm=0.11;rf=0.35;thetam=0.025*365;thetaf=0.03*365;
mu=1/70;theta1=1.2; theta2=1.5; theta3=0.05;
Pim=1000;gammam=3.65;psim=0.5;omegam=0.01;sigmam=0.9;
bm=0.11;rhom=2/52;km=0.5;deltam=0.001;etam=0.05;
Pif=1000;gammaf=1.971;psif=0.5;omegaf=0.01;sigmaf=0.9;
bf=0.11;rhof=2/52;kf=0.25;deltaf=0.001;etaf=0.05;
cm=10;phim=0.5;phif=0.5;

% mu=1/70;theta1=1.2; theta2=1.5; theta3=1.2;
% Pim=1000;gammam=0.5;psim=0.5;omegam=0.01;sigmam=1;
% bm=0.0075;rhom=2/52;km=0.5;deltam=0.1;etam=0.1;rm=0.8;
% Pif=1000;gammaf=0.5;psif=0.5;omegaf=0.01;sigmaf=1;
% bf=0.0075;rhof=2/52;kf=.25;deltaf=0.1;etaf=0.1;rf=0.5;
% taum=0.1;thetam=0.5;tauf=0.1;thetaf=0.5;phim=0.2;phif=0.2;
% cm=12;

dy=[Pim+psim*y(6)-(cm*bf*(y(8)+theta1*y(9)+theta2*y(10)+theta3*y(11))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12))).*y(1)-mu*y(1)
    (cm*bf*(y(8)+theta1*y(9)+theta2*y(10)+theta3*y(11))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12))).*y(1)+rm*(cm*bf*(y(8)+theta1*y(9)+theta2*y(10)+theta3*y(11))/(y(7)+y(8)+y(9)+y(10)+y(11)+y(12))).*y(6)-(mu+rhom+taum*u1)*y(2)
    km*rhom*y(2)+(1-phim)*gammam*y(4)+omegam*sigmam*y(5)-(mu+deltam+u1+thetam)*y(3)
    (1-km)*rhom*y(2)+omegam*(1-sigmam)*y(5)-(mu+deltam+gammam+taum*u1)*y(4)
    u1*y(3)+taum*u1*(y(2)+y(4))-(mu+etam+omegam)*y(5)
    etam*y(5)+phim*gammam*y(4)+thetam*y(3)-(psim+mu+rm*(cm*bf*(y(8)+theta1*y(9)+theta2*y(10)+theta3*y(11))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))).*y(6)
    Pif+psif*y(12)-(((cm*(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))*bm*(y(2)+theta1*y(3)+theta2*y(4)+theta3*y(5))/(y(1)+y(2)+y(3)+y(4)+y(5)+y(6))).*y(7)-mu*y(7)
    (((cm*(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))*bm*(y(2)+theta1*y(3)+theta2*y(4)+theta3*y(5))/(y(1)+y(2)+y(3)+y(4)+y(5)+y(6))).*y(7)+rf*(((cm*(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))*bm*(y(2)+theta1*y(3)+theta2*y(4)+theta3*y(5))./(y(1)+y(2)+y(3)+y(4)+y(5)+y(6))).*y(12)-(mu+rhof+tauf*u2)*y(8)
    kf*rhof*y(8)+(1-phif)*gammaf*y(10)+omegaf*sigmaf*y(11)-(mu+deltaf+u2+thetaf)*y(9)
    (1-kf)*rhof*y(8)+omegaf*(1-sigmaf)*y(11)-(mu+deltaf+gammaf+tauf*u2)*y(10)
    u2*y(9)+tauf*u2*(y(8)+y(10))-(mu+etaf+omegaf)*y(11)
    etaf*y(11)+phif*gammaf*y(10)+thetaf*y(9)-(psif+mu+rf*(((cm*(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))*bm*(y(2)+theta1*y(3)+theta2*y(4)+theta3*y(5))/(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))).*y(12)
    (cm*bf*(y(8)+theta1*y(9)+theta2*y(10)+theta3*y(11))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12))).*y(1)
    (((cm*(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))*bm*(y(2)+theta1*y(3)+theta2*y(4)+theta3*y(5))/(y(1)+y(2)+y(3)+y(4)+y(5)+y(6))).*y(7)
    (((cm*(y(1)+y(2)+y(3)+y(4)+y(5)+y(6)))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12)))*bm*(y(2)+theta1*y(3)+theta2*y(4)+theta3*y(5))/(y(1)+y(2)+y(3)+y(4)+y(5)+y(6))).*y(7) + (cm*bf*(y(8)+theta1*y(9)+theta2*y(10)+theta3*y(11))./(y(7)+y(8)+y(9)+y(10)+y(11)+y(12))).*y(1)];

obj =G1*y(2)+G2*y(3)+G3*y(4)+G4*y(8)+G5*y(9)+G6*y(10)+G7*u1^2+G8*u2^2;