function ConVec=ODEControlOpt(init,ODESoln,CODESoln,var,num,type,opt,vec)

if strcmp(type,'constrained')==1
    A = [-1 0; 0 -1];
    b = [0; 0];
    lb = 0*ones(2,1);
    ub = ones(2,1);
    options = optimset('Display','off','Algorithm','active-set');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fmincon(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,A,b,[],[],lb,ub,[],options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'unconstrained')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fminunc(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'manual')==1
    taum=0.3;tauf=0.3;G7=50;G8=500;
    for i=1:length(CODESoln)
        ConVec(1,i)=max([0,min([1,((CODESoln(3,i)-CODESoln(5,i)).*ODESoln(3,i) +...
                    (CODESoln(2,i)-CODESoln(5,i)).*taum.*ODESoln(2,i) +...
                    (CODESoln(4,i)-CODESoln(5,i)).*taum.*ODESoln(4,i))/(2*G7)])]);
        ConVec(2,i)=max([0,min([1,((CODESoln(9,i)-CODESoln(11,i)).*ODESoln(9,i) +...
                    (CODESoln(8,i)-CODESoln(11,i)).*tauf.*ODESoln(8,i) +...
                    (CODESoln(10,i)-CODESoln(11,i)).*tauf.*ODESoln(10,i))/(2*G8)])]);
    end
end



