function ConVec=ODEControlOpt(init,ODESoln,CODESoln,var,num,type,opt,vec)

if strcmp(type,'constrained')==1
    A = [-1 0 0;0 -1 0;0 0 -1;1 0 0;0 1 0;0 0 1];
    b = [0; 0; 0; 1; 1; 1]; 
    lb = zeros(3,1);
    ub = ones(3,1);
    options = optimset('Display','off','Algorithm','active-set');

    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fmincon(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,A,b,[],[],lb,ub,[],options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'unconstrained')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fminunc(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'manual')==1
    theta1 = 0.0057; theta2 = 2;gamma=2;beta1=0.7; beta2 = 0.1;
    alpha=0.45;l1=0.00384; l2 = 3.1535;  omega = 0.05;
    for i=1:length(CODESoln)
        time = vec(i);
        ConVec(1,i)=(CODESoln(1,i)^(-1/gamma))*(ODESoln(1,i)^(-alpha))*(1+theta1*ODESoln(2,i)^theta2)/(1+omega*ODESoln(4,i));
        ConVec(2,i)=1 - 0.1*(sqrt(-exp(l1*time+l2)*CODESoln(3,i)*CODESoln(1,i)))/(CODESoln(3,i));
        ConVec(3,i)=1 - 0.1*(sqrt((beta1*(1+omega*ODESoln(4,i))*CODESoln(4,i)*CODESoln(1,i)*(ODESoln(1,i)^alpha)/(1+theta1*ODESoln(2,i)^theta2))))/(beta1*CODESoln(4,i));
    end
end

