function [dy, obj]=State(t,y,ConVec)
u=ConVec(1);
a=ConVec(2);
g=ConVec(3);

delta=0.075; gamma=2;
alpha=0.45; lambda=0.11;
eta=0.59;m0=5.964;
theta1 = 0.0057; theta2 = 2;
nu = 0.0054; l1=0.00384; 
l2 = 3.1535;  omega = 0.05; 
beta1=0.7; beta2 = 0.1;

Y=((1+omega*y(4))/(1+theta1*(y(2)^theta2)))*(y(1)^alpha);

c1g = (0.01*g)/(1-g);
c1a = (0.01*a)/(1-a);
%e = 0;
e = exp(l1*t+l2);

dy=[-delta*y(1)+(1-u-c1a-c1g)*Y
    -lambda*y(2)+eta*log(y(3)/m0)
    -nu*y(3)+(1-a)*e*Y
    beta1*g - beta2*y(4)];

obj = ((u*Y)^(1-gamma))/(1-gamma);


