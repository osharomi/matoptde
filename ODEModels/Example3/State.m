function [dy, obj]=State(~,y,ConVec)
C=ConVec(1);
U=ConVec(2);

A=100;alpha1=1;alpha2=1;q=4;theta=0.02;
eps=1;a=1;b=0.01;del1=0.05;del2=0.03;phi=0.04;


Y=A*(alpha1*y(1)^q)/(1+alpha2*y(1)^q);

dy=[(((1-U)^eps)*Y)/(a+b*y(2)^2)-del1*y(1)-C
     theta*(1-U)*Y-del2*y(2)];

obj = log(ConVec(1)*(y(2))^(-phi));


