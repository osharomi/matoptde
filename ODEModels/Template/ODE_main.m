%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  
% Solve 
%        \max\{\phi + \int_0^FT exp(-\rho t)F(x,u,t)\}
% Subject to
%        dx/dt  = g(t,x,u), x(0) = x_0
% 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ODE_main(directory)
% Initial condition, Final condition and number of variables
% numc = number of controls
numc=1;
% FT = Final time
FT=1;
% rho
rho=0;
% Min or Max
opt = 'max';
% dt = Timestep
dt=1/100;
% vec = Time array
vec=0:dt:FT;
% number of iterations
numIter = 100;
% init = initial guess for control
ConVec = ones(numc, length(vec));
% type = 'constrained' (if controls are constraint) 
% and 'unconstrained' (if controls are not constraint) 
% and 'manual' (if controls are known)
type = 'manual';
% MATLAB built-in solver
MatlabSolver='ode15s';
tol = [1e-12 1e-15];
% ODEInit = Initial condition for state variables
ODEInit=1;
% var = number of variables
var=length(ODEInit);
% --------------------------------
oldya = ones(var, length(vec));
oldyb = ones(var, length(vec));
fprintf('%5s %15s %15s %15s %15s %15s \n','Iteration','ControlError',...
                                'StateError','CoStateError','MaxError','IterationTime')
for i=1:numIter
    tic
    oldinit=ConVec;
    ODESoln=ODEForward(ConVec,dt,vec,var,numc,ODEInit,MatlabSolver,tol);
    % ODEFinal = Final condition for co-state variables
    ODEFinal=0;
    %--------------------------------
    CODESoln=ODEBackward(ConVec,ODESoln,dt,vec,var,numc,ODEFinal,rho,...
                         MatlabSolver,tol);  
    %
    ConVec=ODEControlOpt(ConVec,ODESoln,CODESoln,var,numc,type,opt);
    % Error control
    errc=norm(ConVec-oldinit)/norm(ConVec);
    errya=norm(ODESoln-oldya)/norm(ODESoln);
    erryb=norm(CODESoln-oldyb)/norm(CODESoln);
    oldya=ODESoln;oldyb=CODESoln;
    err=max([errc errya erryb]);
    spe=toc;
    fprintf('%5d %18.2e %16.2e %14.2e %16.2e %10g\n',i,errc,errya,erryb,err,spe)
    if (err<1e-6)
        break
    end
    for ii=1:var
        for jj=1:length(vec)
            if isnan(ODESoln(ii,jj))==1 || isnan(CODESoln(ii,jj))==1
                error('Solution diverged')
            end
        end
    end
    for ii=1:numc
        for jj=1:length(vec)
            if isnan(ConVec(ii,jj))==1 || isinf(ConVec(ii,jj))==1
                error('Solution diverged')
            end
        end
    end
end
PlotFunction
directory = strcat(directory,'solution.mat');
save(directory,'ConVec','vec','ODESoln','CODESoln');
