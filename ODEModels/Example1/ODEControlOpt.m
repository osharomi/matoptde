function ConVec=ODEControlOpt(init,ODESoln,CODESoln,var,num,type,opt)

if strcmp(type,'constrained')==1
    A = [1; -1];
    b = [3; 0];
    lb = zeros(1,1);
    ub = 3*ones(1,1);
    options = optimset('Display','off','Algorithm','active-set');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fmincon(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,A,b,[],[],lb,ub,[],options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'unconstrained')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fminunc(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'manual')==1
  %  for i=1:length(CODESoln(1,:))
  %      if CODESoln(1,i)==0
  %          CODESoln(1,i)=CODESoln(1,i+1);
  %      end
  %  end
    ConVec=1./CODESoln(1,:);
    %ConVec=ODEControl(ODESoln, CODESoln);
end