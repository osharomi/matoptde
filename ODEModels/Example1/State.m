function [dy, obj]=State(t,y,ConVec)
%C=ConVec(1);
theta=2;phi=0.1;beta=0.2;alpha=0.2;s=0.06;b=1;deltae=0.04;deltak=0.02;
Y=(y(1)^alpha)*(y(3)^beta)*(y(2)^(1-alpha-beta));

dy=[Y-deltak*y(1)-ConVec(1)
    -s*Y+deltae*y(2)
    y(3)*(theta*y(2)^phi-b*y(3))];

obj = log(ConVec(1)*(y(2))^beta);