%load('solution_1.mat')

subplot(2,2,1); plot(vec,ConVec(1,:),'color','blue');axis tight;
subplot(2,2,2); plot(vec,ConVec(2,:),'color','red');axis tight;
subplot(2,2,3); plot(vec,ODESoln(1,:),'color','blue');axis tight;
subplot(2,2,4); plot(vec,ODESoln(2,:),'color','blue');axis tight;

