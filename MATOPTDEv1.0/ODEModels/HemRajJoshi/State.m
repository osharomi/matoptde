function [dy, obj]=State(t,y,ConVec)
u1=ConVec(1);
u2=ConVec(2);
A1=500000;A2=75;B1=14;B2=1;
s1=2;s2=1.5;mu=0.002;k=2.5e-4;c=0.007;g=30;

dy=[s1 - (s2*y(2))/(B1+y(2)) - mu*y(1) - k*y(2)*y(1) + u1*y(1)
    (g*(1-u2)*y(2))/(B2+y(2))-c*y(2)*y(1)];

obj =y(1)-(A1*u1^2)-(A2*u2^2);