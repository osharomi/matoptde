function ConVec=ODEControlOpt(init,ODESoln,CODESoln,var,num,type,opt,vec)

if strcmp(type,'constrained')==1
    A = [-1 0;0 -1; 1 0;0 1];
    b = [0; 0; 0.02; 0.9];
    lb = zeros(2,1);
    ub = ones(2,1);
    options = optimset('Display','off','Algorithm','active-set');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fmincon(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,A,b,[],[],lb,ub,[],options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'unconstrained')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fminunc(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'manual')==1
    A1=500000;A2=75;B2=1;za=0;zb1=0.02;zb2=0.9;g=30;
    for i=1:length(CODESoln)
        ConVec(1,i)=min([zb1,max([za,(CODESoln(1,i)*ODESoln(1,i))/(2*A1)])]);
        ConVec(2,i)=min([zb2,max([za,(-g*CODESoln(2,i)*ODESoln(2,i))/(2*A2*(B2+ODESoln(2,i)))])]);
    end
end



