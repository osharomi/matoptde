function ConVec=ODEControlOpt(init,ODESoln,CODESoln,var,num,type,opt,vec)

if strcmp(type,'constrained')==1
    A = [-1 0;0 -1; 1 0;0 1];
    b = [0; 0; 1; 1];
    lb = 0.05*ones(2,1);
    ub = 0.95*ones(2,1);
    options = optimset('Display','off','Algorithm','active-set');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fmincon(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,A,b,[],[],lb,ub,[],options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'unconstrained')==1
    options = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton');
    for i=1:length(ODESoln)
        for j=1:num
            initz(j)=init(j,i);
        end
        for j=1:var
            ODEinit(j)=ODESoln(j,i);
            CODEinit(j)=CODESoln(j,i);
        end
        time = vec(i);
        x = fminunc(@(x)ODEHamiltonian(x,ODEinit,CODEinit,opt,time),initz,options);
        ConVec(:,i)=x;
    end
elseif strcmp(type,'manual')==1
    B1=50;B2=500;p=0.4;q=0.1;r1=2;r2=1;za=0.05;zb=0.95;
    for i=1:length(CODESoln)
        ConVec(1,i)=min([zb,max([za,((CODESoln(2,i)-CODESoln(6,i))*r1*ODESoln(2,i))/(B1)])]);
        ConVec(2,i)=min([zb,max([za,(p*(CODESoln(2,i)-CODESoln(6,i))*r2*ODESoln(3,i)+q*(CODESoln(4,i)-CODESoln(6,i))*r2*ODESoln(3,i))/(B2)])]);
    end
end



