function [dy, obj]=State(t,y,ConVec)
u1=ConVec(1);
dy=-y(1)+u1;
obj =0.5*(y(1)^2+u1^2);