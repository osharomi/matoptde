function main
clear;clc
format long
scheme='ODE';
foldername='HemRajJoshi';
directory = strcat('./',scheme,'Models','/',foldername,'/'); 

try
    directory = strcat('./',scheme,'Models','/',foldername,'/'); 
    if strcmp(scheme,'ODE')==1
        addpath(directory)
        addpath('./ODE_Solver/')
    elseif strcmp(scheme,'PDE')==1
        addpath(directory)
        addpath('./PDE_Solver/')  
    end
    if strcmp(scheme,'ODE')==1
        ODE_main(directory)
    elseif strcmp(scheme,'PDE')==1
        PDE_main(directory)
    end
catch exception 
    if strcmp(scheme,'ODE')==1
        rmpath(directory)
        rmpath('./ODE_Solver/')
    elseif strcmp(scheme,'PDE')==1
        rmpath(directory)
        rmpath('./PDE_Solver/')
    end
    rethrow(exception)
end
if strcmp(scheme,'ODE')==1
    rmpath(directory)
    rmpath('./ODE_Solver/')
elseif strcmp(scheme,'PDE')==1
    rmpath(directory)
    rmpath('./PDE_Solver/')
end