function f=ODEHamiltonian(x,ODESoln,CODESoln,opt,time)
if strcmp(opt,'min')==1
    mul=1;
elseif strcmp(opt,'max')==1
    mul=-1;
end
[dy, obj]=State(time,ODESoln,x);
temp = 0;

for i=1:length(ODESoln)
    temp = temp + CODESoln(i)*dy(i);
end
f = mul*(obj+temp);