﻿MATOPTDE ODE Component


To use this toolbox, create a folder inside the ODEModels directory. 

Copy and paste files from Template directory into the newly created directory.

Adjust the files as follows:

State.m: Setup the Ordinary Differential Equation model and its Objective function here. 

ConVec: is the vector holding control variables: where u_i = ConVec(i);
dy represents the RHS of the differential equation;
obj represents the objective function.

ODE_main.m:  Setup the parameters required to solve the optimal control problem here



numc is the number of controls in the model;


FT is the final time;


opt determines if the optimal control problem is been maximized or minimized. ‘max’ for maximize and ‘min’ for minimize;


dt is the time-step;


Convec is the initial guess for control and can be adjusted accordingly. 


type: If the relationship between the control variable and the state/costate variables are known, then type = ‘manual’ otherwise, it can be solved using constrained or unconstrained;


MatlabSolver represents the time scheme used for solving the ODE forward and backward problem. This includes 'ode45', 'ode23', 'ode15s', e.t.c.


ODEInit: represents the initial condition for the forward problem and it’s a vector
of size equal to the number of variables.


ODEFinal: represents the Final condition for the backward problem and it’s a vector
of size equal to the number of variables.



ODEControlOpt.m:  Here, you decide if you are using manual, constrained or unconstrained optimization.

 If the relationship is know, the equation is written in the manual section of this file. Otherwise setup the unconstrained if the variables are not constrained and if they are constrained, then the Matrices A, b, lb and ub are setup. For more information on how to set them up, see the MATLAB manual.



PlotFunction.m: Once the problem is solved, the solution is saved in the directory you have created and it is called ‘solution.mat’. Inside PlotFunction.m, you decide which data to plot. 


Once problem setup is complete, open main.m and set the folder name to the newly created directory and run.

